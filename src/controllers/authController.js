const { Router } = require('express');
const router = Router();

const jwt = require('jsonwebtoken');
const config = require('../config');
const verifyToken = require('./verifyToken');

const User = require('../models/User');

router.post('/signup', async (req, res, next) => {
    const { username, email, password } = req.body;
    const user = new User ({
        username,
        email,
        password
    });
    user.password = await user.encryptPassword(user.password);
    await user.save();

    const token = jwt.sign({id: user._id}, config.secret, {
        expiresIn: 60 * 60 * 24
    })

    res.json({auth: true, token: token})
})

router.get('/me', verifyToken, async (req, res, next) => {    
    const user = await User.findById(req.userId, { password: 0, email: 0 });
    if (!user) {
        return res.status(404).send('El usuario no fue encontrado');
    }

    res.json(user);
})

router.get('/dashboard', verifyToken, (req, res) => {    
    return res.status(404).send('BIENVENIDO');
    //res.json('Bienvenido a dashboard');
})

router.post('/signin', async (req, res, next) => {
    const { email, password } = req.body;
    //console.log(email, password)
    const user = await User.findOne({email: email});
    if (!user) {
        return res.status(404).send('El usuario no existe por favor registrese');
    }
    const validPasword = await user.validatePassword(password);
    if (!validPasword) {
        //return res.status(401).json({auth: false, token: null});
        return res.status(404).send('Contraseña erronea intente de nuevo');
    }

    const token = jwt.sign({id: user._id}, config.secret, {
        expiresIn: 60 * 60 * 24
    });
    res.json({auth: true, token });
})


module.exports = router;